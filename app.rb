#app.rb
require 'sinatra'
require 'sinatra/activerecord'
require 'json'
require 'bcrypt'
require 'date'

set :database, "sqlite3:///banco.db"

class Imagem < ActiveRecord::Base
	self.table_name = 'imagens'
	belongs_to :materias
end

class Video < ActiveRecord::Base
	belongs_to :materias
end

class Materia < ActiveRecord::Base
	self.table_name = 'materias'
	has_many :videos
	has_many :imagens
end

class User < ActiveRecord::Base
	self.primary_key = 'username'
	validates_uniqueness_of :username
end

def parametros_invalidos?(params)
	params['username'].blank? or params['password'].blank?
end

def criar_usuario(params,tipo)
	begin
		halt 400 if parametros_invalidos?(params)
		halt 409 if User.exists?(:username => params['username'])
		password_salt = BCrypt::Engine.generate_salt
	  	password_hash = BCrypt::Engine.hash_secret(params['password'], password_salt)
	 	token = SecureRandom.base64(64)	

	 	if (user = User.create(:username => params['username'],
	 							   :password_hash => password_hash,
	 							   :password_salt => password_salt,
	 							   :tipo => tipo,
	 							   :token => token))
	 		return token	
		end
		return nil
	rescue
  		halt 500
	end
end

def autenticar(params)
	begin
		halt 400 if parametros_invalidos?(params)
		user = User.where(:username => params['username']).first
	  	if !user.nil?
	  		if user.password_hash != BCrypt::Engine.hash_secret(params['password'], user.password_salt).to_s
	  			halt 400 , "Usuario ou senha incorretos"
	  		end
		 	user.token = SecureRandom.base64(64)
		 	user.save
		 	user.token
	    end
	rescue
		halt 500
	end
end

def autorizar(params,metodo)
	halt 400 if params['token'].nil?
	user = User.where(:token => params['token']).first
	halt 401 if user.nil? or 
	halt 401 if (metodo == :post and user.tipo == 'leitor')
	return true 
end

get '/materias' do
	autorizar(params,:get)
	begin
		Materia.all.to_json
	rescue	
		halt 404
	end
end

get '/materia/:id' do
	autorizar(params,:get)
	begin
		Materia.find(params[:id]).to_json
	rescue	
		halt 404
	end
end

get '/materia/:id/videos' do
	autorizar(params,:get)
	begin
		Video.where(:materia_id => params[:id]).to_json
	rescue
		halt 404
	end
end

get '/materia/:id/imagens' do
	autorizar(params,:get)
	begin
		Imagem.where(:materia_id => params[:id]).to_json
	rescue
		halt 404
	end
end

post '/materia' do
	params = JSON.parse(request.env["rack.input"].read)
	autorizar(params,:post)
	begin
		halt 400 if params['titulo'].blank? or params['texto'].blank?
		if (materia = Materia.create(:titulo =>params['titulo'], 
									 :texto =>params['texto']))
			materia.to_json
		else 
			halt 400
		end
	rescue => e
		puts e.class
		halt 500
	end
end

get '/videos' do
	autorizar(params,:get)
	begin	
		Video.all.to_json
	rescue
		halt 404
	end
end

get '/video/:id' do
	autorizar(params,:get)
	begin
		Video.find(params[:id]).to_json
	rescue
		halt 404
	end
end

post '/video' do
	params = JSON.parse(request.env["rack.input"].read)
	autorizar(params,:post)
	begin
		halt 400 if params['url'].blank? or params['materia_id'].blank?
		if (video = Video.create(:url => params['url'], 
								 :materia_id => params['materia_id']))
			video.to_json
		else 
			halt 400
		end
	rescue
		halt 500
	end
end

get '/imagens' do
	autorizar(params,:get)
	begin
		Imagem.all.to_json
	rescue
		halt 404
	end
end

get '/imagem/:id' do
	autorizar(params,:get)
	begin
		Imagem.find(params[:id]).to_json
	rescue
		halt 404
	end
end

post '/imagem' do
	params = JSON.parse(request.env["rack.input"].read)
	autorizar(params,:post)
	begin
		halt 400 if params['url'].blank? or params['materia_id'].blank?
		if (imagem = Imagem.create(:url => params['url'], 
								   :materia_id => params['materia_id']))
			imagem.to_json
		else 
			halt 400
		end
	rescue
		halt 500
	end
end

post "/criar/jornalista" do
	params = JSON.parse(request.env["rack.input"].read)
	token = criar_usuario(params,'jornalista')
	halt 500 if token.nil?
	token
end

post "/criar/leitor" do
	params = JSON.parse(request.env["rack.input"].read)
	token = criar_usuario(params,'leitor')
	halt 500 if token.nil?
	token
end

post "/login" do
	params = JSON.parse(request.env["rack.input"].read)
	autenticar(params)
end