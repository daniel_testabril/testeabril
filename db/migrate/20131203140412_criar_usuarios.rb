class CriarUsuarios < ActiveRecord::Migration
   def up
  	create_table :users do |t|
    	t.string :username
      	t.string :tipo
      	t.binary :password_hash
      	t.binary :password_salt
      	t.string :token
    end
  end

  def down
  	drop_table :users
  end
end
