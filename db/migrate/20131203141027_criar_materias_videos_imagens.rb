class CriarMateriasVideosImagens < ActiveRecord::Migration
  def up
  	create_table :videos do |t|
    	t.string :url
      	t.integer :materia_id
    end

    create_table :imagens do |t|
	  	t.string :url
      	t.integer :materia_id
	end

	create_table :materias do |t|
	  	t.string :titulo
      	t.string :texto
	end
  end

  def down
  	drop_table :videos
  	drop_table :imagens
  	drop_table :materias
  end
end
